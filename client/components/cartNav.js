import React from 'react';
import { render } from 'react-dom';
import _Row from 'antd/lib/row';
import _Col from 'antd/lib/col';
import { Menu, Dropdown, Icon } from 'antd';

const Bioprocess = 'Bioprocess Control';
const WTW = 'WTW';

const menu = (
  <Menu>
    <Menu.Item key="0">
      <a target="_blank" rel="noopener noreferrer" href="#">productA * 3</a>
    </Menu.Item>
    <Menu.Item key="1">
      <a target="_blank" rel="noopener noreferrer" href="http://#">productB * 3</a>
    </Menu.Item>
    <Menu.Item key="2">
      <a target="_blank" rel="noopener noreferrer" href="http://#">productB * 3</a>
    </Menu.Item>
    <Menu.Item key="3">
      <a target="_blank" rel="noopener noreferrer" href="http://#">productB * 3</a>
    </Menu.Item>
    <Menu.Item key="4">
      <a target="_blank" rel="noopener noreferrer" href="http://#">productB * 3</a>
    </Menu.Item>
    <Menu.Divider />
    <Menu.Item key="5" disabled>Back</Menu.Item>
  </Menu>
);

const styles = {
/*  font_bold: {
    fontSize: '19px',
    color: '#06dab5',
    font: 'Arial',
    fontWeight: 'bold'
  },
  font_normal: {
    fontSize: '19px',
    color: '#232528',
    font: 'Arial'
  },*/
  inc: {
    height: '30px',
    weight: '30px',
  },
  bg_gray: {
    backgroundColor: '#F4F4F4',
    width: '100%',
    position: 'fixed',
    zIndex: '9',
    height: '60px',
    marginTop: '60px'
  }
}

const CartNav = React.createClass ({
  render(){
    return (
      <_Row type="flex" justify="space-around" align="middle" style={styles.bg_gray}>
        <_Col span={22} offset={2}>
          <_Row>
             <_Col span={4} offset={6}><a className="cart_nav" href="#">{ Bioprocess }</a></_Col>
             <_Col span={4} offset={2}><a className="cart_nav" href="#">{ WTW }</a> </_Col>
             <_Col span={2} offset={4}>
              <Dropdown overlay={menu}>
                <a className="ant-dropdown-link" href="#">
                  <img style={styles.inc} src="/images/bag.png"/>
                </a>
              </Dropdown>
            </_Col>
          </_Row>
        </_Col>
     </_Row>
    )
  }
});

export default CartNav;
