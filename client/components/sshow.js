import React from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { render } from 'react-dom';
import mobx from 'mobx';
import { observer, inject, toJS } from 'mobx-react';
import request from 'superagent';
import { Row, Col, Button, Form, Input } from 'antd';
import _Card from 'antd/lib/card';
import cookie from 'react-cookie';

const FormItem = Form.Item;
const name= "100ml 玻璃瓶 B1";
const description= "Glass bottle 100ml";
const descriptor= "AMPTS II (CO2吸收单元)";
const price= "￥1000";
const button_buy= "购  买";
const button_load= "加载更多";

const styles = {
  fontbold:{
    fontWeight: 'bold',
    fontSize: '23px'
  },
  fontprice:{
    fontWeight: 'bold',
    fontSize: '20px'
  },
  straightline: {
    position: 'relative',
    content: '',
    marginBottom: '20px',
    width: '230px',
    height: '1px',
    backgroundColor: '#777'
  },
  fontmiddle:{
    fontSize: '14px',
    marginBottom: '20px',
  },
  imgsize:{
    width: '243px',
    height: '353px',
    marginBottom: '20px',
  },
  button_buy:{
   width:'80px',
   height:'35px',
   fontSize:'16px',
   paddingBottom:'10px'
  },
  button_load:{
   width:'100px',
   height:'40px',
   fontSize:'17px'
  },
  rowspace:{
    padding:'20px 10px',
  }
}

/*const Product = function Product(props) {
  function addCookieShopcar() {
    console.debug('addCookie');
    cookie.save('productionId', Math.random(), { path: '/' });
    console.log('add coockie done');
  }

  function render() {
    return (
      <_Card bodyStyle={{ padding: 0, textAlign: 'center' }}>
        <div className="custom-card">
          <h3 style={ styles.fontbold }>{ props.name }</h3>
          <p style={ styles.fontmiddle }>{ props.description }  {descriptor} </p>

        </div>
        <div>
          <img style={{margin: 'auto auto'}} src="http://fakeimg.pl/150x150" />
        </div>
        <p >{ price }</p>
        <p>
          <Button type="ghost" onClick={addCookieShopcar}>{ button_buy }</Button>
        </p>
      </_Card>
    )
  }
  return render();
}*/

const ProductList = Form.create()(inject('store')(function ProductList (props){
  function componentDidMount(){
  /*request
   .post('/api/pet')
   .send({ name: 'Manny', species: 'cat' })
   .set('X-API-Key', 'foobar')
   .set('Accept', 'application/json')
   .end(function(err, res){
     if (err || !res.ok) {
       alert('Oh no! error');
     } else {
       alert('yay got ' + JSON.stringify(res.body));
     }
   });*/
  }
  function handleSubmit(e) {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        props.store.addCookieShopcar(values);
      }
    });
  }

  function render(){
    const { getFieldDecorator } = this.props.form;
    return(
        <Row style={styles.rowspace}>
          { props.store.sampleList.map(function(num){
            const accessoriesName=num.accessoriesName;
            return(
              <Col span={4} offset={1}>
                <_Card bodyStyle={{ padding: 0, textAlign: 'center' }}>
                 <Form onSubmit={ handleSubmit }>
                  <div className="custom-card">
                    <h3 style={ styles.fontbold }>{ num.accessoriesName }</h3>
                    <FormItem style={styles.formItemMargin}>
                      {getFieldDecorator( accessoriesName, accessoriesName )}
                     </FormItem>
                    <p style={ styles.fontmiddle }>{ num.accessoriesDescription }  {descriptor} </p>
                  </div>
                  <div>
                    <img style={{margin: 'auto auto'}} src="http://fakeimg.pl/150x150" />
                  </div>
                  <p >{ price }</p>
                  <p>
                      <Button type="ghost" htmlType="submit">{ button_buy }</Button>
                  </p>
                  </Form>
                </_Card>
              
              </Col>
            )
          }) 
        }   
        </Row>
      )
    }
    return observer({
    componentDidMount,
    render,
  });
}));

const Sshow = (inject('store')(function Sshow (props){
    function componentDidMount(){
    console.log('Sshow mount', this);
    request.get('/api/accessories')
      .end(function(err, res){
      console.log('list add successful');
      props.store.setSampleList(res.body);
    })
  }

  function addCookieShopcar() {
    console.debug('addCookie');
    cookie.save('productionId', Math.random(), { path: '/' });
    console.log('add coockie done');
  }


  function render(){
    return (
        <div style={{ paddingTop: '200px', 'paddingBottom':'60px'}}>
          {/*<Row>
            <Col span={4} offset={2} >
              <Product/>
            </Col>
            <Col span={4} offset={1}>
               <Product/>
            </Col>
            <Col span={4} offset={1}>
              <Product/>
            </Col>
            <Col span={4} offset={1}>
             <Product/>
            </Col>
          </Row>
          <Row style={styles.rowspace}>
            <Col span={4} offset={2} >
              <Product/>
            </Col>
            <Col span={4} offset={1}>
               <Product/>
            </Col>
            <Col span={4} offset={1}>
              <Product/>
            </Col>
            <Col span={4} offset={1}>
             <Product/>
            </Col>
          </Row>*/}
          <Row style={styles.rowspace}>
            <ProductList></ProductList>
          </Row>
          <Row>
            <Col span={3} offset={11}> <Button type="ghost" style={styles.button_load}>{ button_load }</Button> </Col>
          </Row>
      </div>
    )
  }
return observer({
    componentDidMount,
    render,
  });
}));
export default Sshow;