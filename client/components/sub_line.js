import React from 'react';
import { render } from 'react-dom';
import { Row, Col} from 'antd';

const greenhand='新手入门';
const industryapplication='行业应用';
const question='常见问题';
const maintein='维护保养';
const repairs='设备维修';
const integrate='设计集成';
const rent='设备租赁';

const styles = {
  line:{
    marginBottom:'10px'
  },
  bg_gray:{
    backgroundColor:'#F4F4F4',
    width:'100%',
    position: 'fixed',
    zIndex:'9',
  },
   top:{
    height:'72px'
  }
}

const Sub_line = React.createClass ({
  render(){
    return (
      <Row style={styles.bg_gray}>
        <Col style={styles.top}></Col>
        <Row type="flex" justify="space-around" style={styles.line}>
           <Col span={12}>
             <Col span={3} ><a className="subline_label" href="/beginnersguide">{ greenhand }</a></Col>
             <Col span={3} ><a className="subline_label" href="/solutions">{ industryapplication }</a> </Col>
             <Col span={3} ><a className="subline_label" href="/faq">{ question }</a></Col>
             <Col span={3} ><a className="subline_label" href="/care">{ maintein }</a></Col>
             <Col span={3} ><a className="subline_label" href="/service">{ repairs }</a> </Col>
             <Col span={3} ><a className="subline_label" href="/design">{ integrate }</a></Col>
             <Col span={3} ><a className="subline_label" href="/rent">{ rent }</a></Col>
           </Col>
        </Row>
      </Row>
    )
  }
});
export default Sub_line;