#Novaskantek-PublicWeb

## Front component:
https://ant.design/docs/react/introduce

## TEST
* [/home]
* [/admin]
* [/cart] - pdf 
* [/shopshow]
* [/rent]
* [/service]
* [/test]
* [/solutions]
* [/design]
* [/jobs]
* [/faq]
* [/shows]
* [/lewa]
* [/beginnersguide]
* [/care]

## DEV

http://biogas360.herokuapp.com/

## SITE-MAP
* [Home](http://biogas360.herokuapp.com)
* [Admin](http://biogas360.herokuapp.com/admin)
* [Cart](http://biogas360.herokuapp.com/cart)
* [shopshow](http://biogas360.herokuapp.com/shopshow)
* [deviceborrow](http://biogas360.herokuapp.com/deviceborrow)
* [deviceconsult](http://biogas360.herokuapp.com/deviceconsult)
* [devicetest](http://biogas360.herokuapp.com/devicetest)
* [industrycases](http://biogas360.herokuapp.com/industrycases)
* [industrysamples](http://biogas360.herokuapp.com/industrysamples)
* [programmes](http://biogas360.herokuapp.com/programmes)
* [questions](http://biogas360.herokuapp.com/questions)
* [shows](http://biogas360.herokuapp.com/shows)
* [joins](http://biogas360.herokuapp.com/joins)
* [greenhand](http://biogas360.herokuapp.com/greenhand)
* [maintain](http://biogas360.herokuapp.com/maintain)